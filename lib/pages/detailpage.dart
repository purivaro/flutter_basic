import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  final int id;

  DetailPage(this.id);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Basic'),
      ),
      body: Column(
        children: <Widget>[
          Text('Detail'),
          RaisedButton(
            child: Text('Back'),
            onPressed: () => Navigator.pop(context),
          )
        ],
      ),
    );
  }
}
