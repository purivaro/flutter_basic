import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:flutter_cache_store/flutter_cache_store.dart';

import 'package:basic/widgets/contactcard.dart';

class IbsList extends StatefulWidget {
  @override
  _IbsListState createState() => _IbsListState();
}

class _IbsListState extends State<IbsList> {


  List<ContactCard> cards = [];
  List<ContactCard> filtered_cards = [];
  final _refreshIndicatorKey = GlobalKey<ScaffoldState>();

  TextEditingController searchController = TextEditingController();
  String searchString = '';
  bool showSearch = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    searchController.addListener(searchListener);
    loadData();
  }

  searchListener() {
    // print(searchController.text);
    // filtered_cards = cards.where((i)=> i.fullname==searchController.text).toList();
    // print(filtered_cards);

    setState(() {
      if (searchController.text != null) {
        if (searchController.text.length > 0) {
          filtered_cards = cards.where((i)=> (
              i.fullname.startsWith(searchController.text) || 
              i.cNickname.startsWith(searchController.text) ||
              i.fullname.startsWith('พระ'+searchController.text)
            )
          ).toList();
        } else {
          filtered_cards = cards;
        }
      }else{
        filtered_cards = cards;
      }
    });
  }



  Future loadData() async {
    final store = await CacheStore.getInstance();
    final file = await store.getFile(
        // 'https://www.vstarproject.com/api/ibsone/contact/json/json_contact_list_unique.php?nId_department_filter=681');
        'https://www.vstarproject.com/api/ibsone/contact/json/json_contact_list_unique.php?');
    // print(file.readAsStringSync());
    final jsondata = json.decode(file.readAsStringSync());

    setState(() {
      cards.clear();
      jsondata['contacts'].forEach((contact) {
        final card = ContactCard.fromJson(contact);
        cards.add(card);
      });
      filtered_cards = cards;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('IBS CONTACT'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              setState(() {
                showSearch = !showSearch;
                if(!showSearch){
                  filtered_cards = cards;
                }else{
                  searchListener();
                }
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              loadData();
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        child: Column(
          children: <Widget>[
            showSearch ? TextField(
              decoration: InputDecoration(
                labelText: 'Search',
                suffixIcon: IconButton(
                  icon: Icon(Icons.clear),
                  onPressed: () {
                    setState(() {
                      searchController.text = '';
                    });
                  }
                ),
              ),
              controller: searchController,
            ) : SizedBox(),
            Expanded(
              child: ListBasic(data: filtered_cards),
            ),
          ],
        ),
        onRefresh: loadData,
      ),
    );
  }
}

class ListBasic extends StatelessWidget {
  final data;
  ListBasic({this.data});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, i) {
        return data[i];
      },
    );
  }
}
