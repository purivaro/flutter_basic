import 'package:flutter/material.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:basic/pages/detailpage.dart';

class ContactCard extends StatelessWidget {
  final int id;
  final String coverImg;
  final String coverImgLg;
  final String fullname;
  final String cFname;
  final String cLname;
  final String cMobile;
  final String cDepartment;
  final String cDepartmentMini;
  final String lineId;
  final String dateOfBirth;
  final String cNickname;

  ContactCard({
    this.id,
    this.coverImg,
    this.coverImgLg,
    this.fullname,
    this.cFname,
    this.cLname,
    this.cMobile,
    this.cDepartment,
    this.cDepartmentMini,
    this.lineId,
    this.dateOfBirth,
    this.cNickname,
  });

  ContactCard.fromJson(Map<String, dynamic> json)
      : coverImg = json['uProfile_img_url'],
        coverImgLg = json['uProfile_lg_img_url'],
        id = int.parse(json['nId_person']),
        fullname = "${json['cFname']} ${json['cLname']}",
        cFname = json['cFname'],
        cLname = json['cLname'],
        cNickname = json['cNickname'],
        cDepartment = json['cDepartment'],
        cDepartmentMini = json['cDepartment_mini'],
        lineId = json['line_id'],
        dateOfBirth = "${json['dBirthday']} ${json['dBirthmonth_thaimini']}",
        cMobile = json['cMobile'];

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        // margin: EdgeInsets.all(5),
        // elevation: 3,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 0),
          child: Row(
            children: <Widget>[
              Container(
                child: CachedNetworkImage(
                  imageUrl: '$coverImg',
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  // placeholder: (context, url) =>
                  // new CircularProgressIndicator(),
                  errorWidget: (context, url, error) => new Icon(Icons.error),
                ),
                height: 80,
                width: 70,
              ),
              Expanded(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '$fullname',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                      Text('$cNickname'),
                      Text('$cDepartment'),
                      Text('$cMobile'),
                    ],
                  ),
                ),
              ),
              Container(
                height: 50,
                width: 50,
                child: GestureDetector(
                  onTap: () {
                    _launchURL("tel:$cMobile");
                  },
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/phonecall.png'),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                height: 50,
                width: 50,
                child: GestureDetector(
                  onTap: () {
                    _launchURL("http://line.me/ti/p/~$lineId");
                  },
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/line.png'),
                      ),
                    ),
                  ),
                ),
              ),
            ],
            //
          ),
        ),
      ),
    );
  }
}
