import 'package:flutter/material.dart';
import 'package:basic/common/theme.dart';
// import './pages/homepage.dart';
import 'package:basic/pages/ibslist.dart';
// import 'package:basic/pages/detailpage.dart';

void main() => runApp(BasicApp());

class BasicApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic Flutter',
      home: IbsList(),
      theme: appTheme,
      // initialRoute: '/',
      // routes: {
      //   '/': (context) => IbsList(),
      //   '/detail': (context) => DetailPage(3),
      // },
    );
  }
}
